//
//  ViewController.swift
//  StorageExample
//
//  Created by Steffen Kern on 17.06.15.
//  Copyright (c) 2015 Steffen Kern. All rights reserved.
//

import UIKit

class ViewController: UIViewController {

    let fileName = "text.data"
    let dataDir = "data"
    let pathSeparator = "/"
    
    func saveToDisk()
    {
        let paths = NSSearchPathForDirectoriesInDomains(.DocumentDirectory, .UserDomainMask, true)[0] as! String
        let dataDirPath = paths.stringByAppendingPathComponent( dataDir )
        let filePath = paths.stringByAppendingPathComponent( dataDir + pathSeparator + fileName )
        
        let fileManager = NSFileManager.defaultManager()
        
        // If not present so far, we create the data directory
        fileManager.createDirectoryAtPath( dataDirPath, withIntermediateDirectories: true, attributes: nil, error: nil )
        
        let text = "any string you want"
        
        //writing
        text.writeToFile( filePath, atomically: false, encoding: NSUTF8StringEncoding, error: nil);
        
    }
    
    func loadFromDisk()
    {
        let paths = NSSearchPathForDirectoriesInDomains(.DocumentDirectory, .UserDomainMask, true)[0] as! String
        let filePath = paths.stringByAppendingPathComponent( dataDir + pathSeparator + fileName )
        
        let fileManager = NSFileManager.defaultManager()
        
        if (fileManager.fileExistsAtPath( filePath ) )
        {
            let text = String(contentsOfFile: filePath, encoding: NSUTF8StringEncoding, error: nil)
            
            NSLog(text!)
        }
        
    }
    
    override func viewDidLoad()
    {
        super.viewDidLoad()
        
        self.saveToDisk()
        self.loadFromDisk()
        
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }


}

