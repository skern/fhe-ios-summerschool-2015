//
//  ViewController.swift
//  MapKitExample
//
//  Created by Steffen Kern on 17.06.15.
//  Copyright (c) 2015 Steffen Kern. All rights reserved.
//

import UIKit
import MapKit

class ViewController: UIViewController {

    @IBOutlet weak var mapView: MKMapView!
    
// MARK: - Map Configuration
    
    func initMapView()
    {
        // Create a Location
        // The CoreLocation Framework (e.g. GPS) will also produce CLLocationCoordinate2D objects
        let location = CLLocationCoordinate2D(
            latitude: 51.50007773,
            longitude: -0.1246402
        )
        
        // Set the region to be shown on the map
        let span = MKCoordinateSpanMake( 0.05, 0.05)
        let region = MKCoordinateRegion( center: location, span: span )
        self.mapView.setRegion( region, animated: true )
        
        // Add a marker for the location defined above
        let annotation = MKPointAnnotation()
        annotation.coordinate = location
        annotation.title = "Big Ben"
        annotation.subtitle = "London"
        mapView.addAnnotation( annotation )
    }
    
    
// MARK: - View Controller Life Cycle
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Init the Map View
        self.initMapView()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }


}

