//
//  DetailViewController.swift
//  CoreDataExample
//
//  Created by Steffen Kern on 18.06.15.
//  Copyright (c) 2015 Steffen Kern. All rights reserved.
//

import UIKit

class DetailViewController: UIViewController {

    @IBOutlet weak var textEntryField: UITextField!

    
    @IBAction func saveEntry(sender: AnyObject)
    {
        // Update Managed Object
        self.detailItem?.setValue(self.textEntryField.text, forKey: "title")
        
        // Save context to store changes
        let app = UIApplication.sharedApplication().delegate as! AppDelegate
        app.saveContext()
        
        // Go back to the previous view controller
        self.navigationController?.popViewControllerAnimated(true)
    }
    
    
    var detailItem: AnyObject? {
        didSet {
            // Update the view.
            self.configureView()
        }
    }

    func configureView() {
        // Update the user interface for the detail item.
        if let detail: AnyObject = self.detailItem {
            if let textEntry = self.textEntryField {
                textEntry.text = detail.valueForKey("title")!.description
            }
        }
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
        self.configureView()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }


}

