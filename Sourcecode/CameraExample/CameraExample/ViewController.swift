//
//  ViewController.swift
//  CameraExample
//
//  Created by Steffen Kern on 17.06.15.
//  Copyright (c) 2015 Steffen Kern. All rights reserved.
//

import UIKit
import MobileCoreServices

class ViewController: UIViewController, UIImagePickerControllerDelegate, UINavigationControllerDelegate
{
    
    // Some constants for the image name as well as the image directory
    let imageName = "imageName.jpg"
    let imageDir = "images"
    let pathSeparator = "/"
    
    // A reference to our UIImageView that will show the images taken
    @IBOutlet weak var imageView: UIImageView!
    
// MARK: - Button Action Methods

    @IBAction func takeImage(sender: AnyObject)
    {
        self.showImagePicker( UIImagePickerControllerSourceType.Camera )
    }
    
    @IBAction func pickImage(sender: AnyObject)
    {
        self.showImagePicker( UIImagePickerControllerSourceType.PhotoLibrary )
    }
    
    func showImagePicker( pickerType: UIImagePickerControllerSourceType )
    {
        // Check if the device provides the requested type, e.g. a camera or the photo library
        // Photo Library will work in the simulator, camera wount
        if UIImagePickerController.isSourceTypeAvailable( pickerType )
        {
            let imagePicker = UIImagePickerController()
            
            // Configure the image picker
            imagePicker.delegate = self
            imagePicker.sourceType = pickerType
            imagePicker.mediaTypes = [kUTTypeImage]
            imagePicker.allowsEditing = false
            
            // Show image picker
            self.presentViewController(imagePicker, animated: true, completion: nil)
        }
    }
    
// MARK: - Image Picker Delegate
    
    // Delegate method that is called if the user actually picked an image
    func imagePickerController(picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [NSObject : AnyObject])
    {
        let mediaType = info[UIImagePickerControllerMediaType] as! NSString
        
        // Check if the acquired media is an image and not a video
        if mediaType.isEqualToString(kUTTypeImage as! String)
        {
        
            let image = info[UIImagePickerControllerOriginalImage] as! UIImage
            
            // If you allowed for editing the image after taking it, you can acquire the edited image using
            // let image = info[UIImagePickerControllerEditedImage] as! UIImage
            
            // Show image in UIImageView
            self.imageView.image = image
            
            // Save image to disk
            self.saveImage( image )
        }
        
        // Dismiss the Image Taking UI
        self.dismissViewControllerAnimated(true, completion: nil)
    }
    
    // Delegate method that is called if the user canceled the image taking process
    func imagePickerControllerDidCancel(picker: UIImagePickerController)
    {
        // Dismiss the Image Taking UI
        self.dismissViewControllerAnimated(true, completion: nil)
    }
    
// MARK: - Image Storage Handling
    
    // Save image to local app storage
    func saveImage( image: UIImage )
    {
        let paths = NSSearchPathForDirectoriesInDomains(.DocumentDirectory, .UserDomainMask, true)[0] as! String
        let dirPath = paths.stringByAppendingPathComponent( imageDir )
        let imagePath = paths.stringByAppendingPathComponent( imageDir + pathSeparator + imageName )
        let fileManager = NSFileManager.defaultManager()
        
        // Saving should be done in a separate thread - thus we do not block the UI during the process
        dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_BACKGROUND, 0))
        {
            // If not present so far, we create the image directory
            fileManager.createDirectoryAtPath(dirPath, withIntermediateDirectories: true, attributes: nil, error: nil)
            
            // Get a JPEG representation of the image and write it to disk
            UIImageJPEGRepresentation(image, 100).writeToFile(imagePath, atomically: true)
            
            NSLog("Image written to disk")
        }
    }
    
    // Load the image from disk
    func loadImage()
    {
        let paths = NSSearchPathForDirectoriesInDomains(.DocumentDirectory, .UserDomainMask, true)[0] as! String
        let imagePath = paths.stringByAppendingPathComponent( imageDir + pathSeparator + imageName )
        let fileManager = NSFileManager.defaultManager()
        
        if (fileManager.fileExistsAtPath(imagePath))
        {
            let loadedImage = UIImage(contentsOfFile: imagePath)
            self.imageView.image = loadedImage;
        }
    }
    
    
// MARK: - View Controller Life Cycle
    
    override func viewDidLoad()
    {
        super.viewDidLoad()
        
        // Load last image taken
        self.loadImage()
    }
    
    override func didReceiveMemoryWarning()
    {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
}

